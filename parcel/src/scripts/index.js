import {getFullName} from "./utils"
import '../styles/index.scss'

const firstName = getFullName( 'Daniel Scicluna' )

console.log( firstName )

class Name {
  name = 'Anonymous'
}
