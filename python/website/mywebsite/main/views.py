from django.shortcuts import render, get_object_or_404, redirect
from news.models import News
from .models import Main

# Create your views here.


def home(request):

    # sitename = "MySite | Home"

    # site = Main.object.all()
    # site = Main.objects.filter(pk=2) //returns a Dictionary - needs a loop to access fields
    site = Main.objects.get(pk=2)

    # fields can be accessed like so:
    # sitename = site.name
    # sitename = Main.objects.get(pk=2).name

    news = News.objects.all()

    return render(request, 'front/home.html', {'site': site, 'news': news})


def about(request):

    site = Main.objects.get(pk=2)

    return render(request, 'front/about.html', {'site': site})

def panel(request):

    return render(request, 'back/home.html')
