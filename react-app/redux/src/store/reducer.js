// ##1
// this reducer is not used anymore since we are using the multi reducer example
import * as actionTypes from "./actions"

const initialScale = {
    counter: 0,
    results: []
}

const reducer = (state = initialScale, action) => {

    switch (action.type) {
        case actionTypes.INCREMENT:
            return {
                ...state,
                counter: state.counter + 1
            }
        case actionTypes.DECREMENT:
            return {
                ...state,
                counter: state.counter - 1
            }
        case actionTypes.ADD:
            return {
                ...state,
                counter: state.counter + action.value
            }
        case actionTypes.SUBTRACT:
            return {
                ...state,
                counter: state.counter - action.value
            }
        case actionTypes.STORE_RESULT:
            return {
                ...state,
                results: state.results.concat({id: new Date(), value: state.counter})
            }
        case actionTypes.DELETE_RESULT:

            //how to change an array immutably
            // const id = 2;
            // const newArray = [...state.results];
            // state.results.splice(id, 1);

            //filter puts in the updatedArray all elements that return true
            const updatedArray = state.results.filter(result => result.id !== action.resultElID);

            return {
                ...state,
                results: updatedArray
            }
        default:
            return state
    }
}

export default reducer