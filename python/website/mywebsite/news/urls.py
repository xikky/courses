from django.conf.urls import url
from . import views

urlpatterns = [

    # url(r'^news/(?P<pk>\d+)/$', views.news_details, name='news_details')
    # url(r'^news/(?P<word>\w+)/$', views.news_details, name='news_details')
    #the .* is used to parse a string to url: ex test%201 from test 1
    url(r'^news/(?P<word>.*)/$', views.news_details, name='news_details'),
    url(r'^panel/news/list/$', views.news_list, name='news_list'),
    url(r'^panel/news/add/$', views.news_add, name='news_add'),
    url(r'^panel/news/del/(?P<pk>\d+)/$', views.news_delete, name='news_delete')
]