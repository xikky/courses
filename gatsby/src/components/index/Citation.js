import React from 'react';
import {useStaticQuery, graphql} from 'gatsby';

import citatimg from '../../images/tango_citat.svg';
import {CitatWrapper} from "./styles/CitatStyles";

const Citation = () => {

  const data = useStaticQuery( graphql`
      query{
          wordpressPage(wordpress_id: {eq: 45}) {
              acf {
                  text
                  author
              }
          }
      }
  ` )

  return (
    <CitatWrapper>
      <div className="container">
        <div className="row">
          <div className="col">
            <img src={citatimg} alt="quote"/>
            <h6>{data.wordpressPage.acf.text}</h6>
            {data.wordpressPage.acf.author}
          </div>
        </div>
      </div>
    </CitatWrapper>
  )
}

export default Citation