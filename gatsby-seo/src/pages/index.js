import React from "react"
import {graphql} from "gatsby";

import PrimaryLayout from "../layouts/PrimaryLayout"
import Post from "../components/Post";

export default ( { data } ) => {

  return (
    <PrimaryLayout>
      {data.allWordpressPost.nodes.map( ( node ) => {

        if ( node.featured_media ) {

          return <Post
            image={node.featured_media.source_url}
            title={node.title}
            excerpt={node.excerpt}
            readMore={node.slug}></Post>;
        }

        return null;
      } )}
    </PrimaryLayout>
  )
}

export const query = graphql`
    {
        allWordpressPost {
            nodes {
                slug
                title
                excerpt
                featured_media {
                    source_url
                }
            }
        }
    }
`