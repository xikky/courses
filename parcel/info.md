# Parcel

- parcel is a zero configuration module bundler
- parcel needs to be globally installed to run CLI commands
- alternatively it can be installed in project scope and run commands through 'scripts' from package.json
- JS source files on the browser need a manual refresh to change converted JS code to the actual developed code

### To customize Babel configuration ( [babel website](http://www.babeljs.io) ):
- by default Parcel uses preset-env
- to install Babel plugins do ex: `npm install @babel/plugin-proposal-class-properties`
- create `.babelrc` file and add the support for the plugin

### Styling
- to add support for sass install npm package `npm install sass`
- import the files in JS: `import '../styles/index.scss'`