# JSX

- use `let` or `const` instead of `var`
- arrow function equivalent to normal function is:
    ```javascript
    //normal function
    function getMyName(name){
    return "My name is" + name;
    }

    //arrow function
    const getMyName = (name) => {
    return "My name is" + name;
    }

    //shorter arrow function. Only if one param is passed, and if one return line makes the function body
    const getMyName = name => "My name is" + name;
    ```
- modules: `import` and `export` keywords
    ```
    //person.js
    const person = {
    name: 'Max'
    }

    export default person

    //utility.js
    export const clean = () => {}

    export const baseData = 10;

    //app.js
    //notice the difference when the 'default' keyword is applied after export
    import my_person_object from './person.js'

    import {baseData} from './utility.js'
    import {clean} from './utility.js'

    //also
    import {baseData, clean} from './utility.js'
    //also
    import {baseData as myData} from './utility.js'
    //also
    import * as bundled from './utility.js'
    ```
- classes:
    ```
    class Human {

        //ES6 way of setting class properties
        constructor() {
            this.gender = 'male'
        }

        //ES7 way of setting class properties
        gender = 'male'

        //ES6 way of setting class method
        printGender() {
            console.log(this.gender)
        }

        //ES7 way of setting class method. The advantage is that the this keyword is surely scoped within this function
        printGender = () => console.log(this.gender)
    }

    class Person extends Human {
        constructor() {
            super(); // IMPORTANT: to execute the parent constructor
            this.name = 'Max'
            this.gender = 'female'
        }

        printMyName() {
            console.log(this.name)
        }
    }

    const person = new Person()
    person.printMyName()
    person.printGender()
    ```
- spread or rest operators - `...` :
    ```
    //spread - used to split up array elements OR object properties
    //add 1 and 2 to oldArray into newArray
    const newArray = [ ...oldArray, 1, 2 ]
    //add newProp or replace newProp property value of oldObject into newObject
    const newObject = { ...oldObject, newProp.5 }

    //rest - used to merge a list of function arguments into an array
    //merge parameters into an array
    function sortArgs = (...args) => {
        return args.sort()
    }

    const newArray = sortArgs( 1, 3, 2 )
    ```
- destructuring - extracting array elements or object properties and store them in variables
    ```
    //array destructuring
    [a,b] = ['Hello', 'Max']
    console.log(a) //Hello
    console.log(b) //Max

    //object destructuring
    {name} = {name: 'Max', age: 28}
    console.log(name) //Max
    console.log(age) //undefined

    //skip an element or a property
    const numbers = [1, 2, 3]
    [num1, , num3] = numbers; //NOTE: the space and comma to skip element or property
    console.log(num1, num3) //1 3
    ```
- reference and primitive types - objects and arrays vs variables
    ```
    //primitive
    const number = 1;
    const num2 = number;

    console.log(num2); //1

    //reference
    const person = {
        name: 'Max'
    }

    //the pointer of the person object is copied to secondPerson and not the object itself
    const secondPerson = person

    //the actual object is changed, thus also the variables value containing pointers for this object
    person.name = 'Manu'

    //prints 'Manu' because it has been changed even though secondPerson is stored before the actual change of the property
    console.log(secondPerson) //Manu

    //this is an actual copy of the person object and not just the pointer
    const thridPerson = {
        ...person
    }
    ```
- array functions
    ```
    const numbers = [1, 2, 3]

    //on each array element execute the map function
    const doubleNumArray = numbers.map((num) => {
        return num * 2;
    });

    console.log(numbers) //1, 2, 3
    console.log(doubleNumArray) //2, 4, 6
    ```

---

# React

- install `create-react-app` using npm globally on the machine. This will setup a react project, installing `react`, `react-dom`, `react-scripts`
- stateless vs stateful components
    - stateful
        - referred to as containers
        - is a class - `class XY extends Component`
        - has access to state
        - can implement lifecycle hooks
        - can use `this` keyword
        - use only if you need to manage state or access to lifecycle hooks!
    - stateless
        - is a function - `const XY = (props) => {...}`
        - no access to state
        - cannot implement lifecycle hooks
        - use `props.XY`
        - use in all other cases
 - component lifecycle ( legacy lifecycle - new lifecycle see below )
    - only available in stateful components ( below, cause side-effects refers to make use of third party requests )
        - creation
            - constructor( props ) - call super(props) / DO set up state / DON'T cause side-effects
            - componentWillMount() - DO update state, last minute optimization / DON'T cause side-effects
            - render()
            - ...render child components
            - componentDidMount() - DO  cause side effects / DON'T update state
        - triggered by parent
            - componentWillReceiveProps( nextProps ) - DO sync state to props / DON'T cause side-effects
            - shouldComponentUpdate( nextProps, nextState ) - DO decide whether to continue or not / DON'T cause side-effects
            - componentWillUpdate( nextProps, nextState ) - DO sync state to props / DON'T cause side-effects
            - render()
            - ...update child components
            - componentDidUpdate() - DO cause side-effects / DON'T update state
        - triggered by internal change
            - shouldComponentUpdate( nextProps, nextState ) - DO decide whether to continue or not / DON'T cause side-effects
            - componentWillUpdate() - DO sync state to props / DON'T cause side-effects
            - render()
            - ...update child component props
            - componentDidUpdate() - DO cause side-effects / DON'T update state
        - others
            - componentDidCatch()
            - componentWillUnmount()
 - componenet lifecycle ( new lifecycle - missing hooks here can be used but might not in the future )
    - only available class-based components ( below, cause side-effects refers to make use of third party requests )
      - creation
        - constructor() - receives the props / call super(props) / DO set up state / DON'T cause side-effects 
        - getDerivedStateFromProps() - ( added 16.3 ) / use when props change, sync your state to them / DON'T cause side-effects
        - render() - prepare & structure your JSX code
        - ..render child components
        - componentDidMount() - DO cause side-effects / DON'T update state ( unless as a promise from side-effects )
    - update
        - getDerivedStateFromProps( props, state ) - update state upon prop changes ( rarely used ) / DO Sync State to Props / DON'T cause slide-effects
        - shouldComponentUpdate( nextProps, nextState ) - allows to cancel the update process - for performance optimization / DO decide whether to continue or not / DON'T cause side-effects
        - render() - prepare & structure your JSX code
        - update child component props
        - getSnapshotBeforeUpdate( prevProps, prevState ) - DO last-minute DOM ops / DON'T cause side-effects ( use for like scroll back )
        - componentDidUpdate() - DO cause side-effects / DON'T update state ( watch out of infinite loop in case of updating same component )
      - other       
        - componentDidCatch()
        - componentWillUnmount()

