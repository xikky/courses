//Stying example is used just for example, good to use hoc for error handling

//##12
//File name was renamed to lowerCamelCase since it contains a normal JS function and not a component anymore

import React from 'react';

//====> A functional component // using () no return
//##12
// const withClass = props => (
//     <div className={props.classes}>
//         {props.children}
//     </div>
// );

//this can accept as many args as you want
//====> A normal JS Function // using {} with return
//##12
const withClass = (WrappedComponent, className) => {

    //returns a functional component
    return props => (
        <div className={className}>
            <WrappedComponent {...props}/>
        </div>
    )
};

export default withClass;