const redux = require( "redux" )
const createStore = redux.createStore

const initialState = {
  counter : 0,
}

// Reducer
// set a default state ( initialState ) in case state is undefined ( initially it is undefined )
// there can be multiple reducers
// reducers are responsible to change the 'central store' ( the state )
// the update of the state from the received action happens sync ( there is a way to do it async )
const rootReducer = ( state = initialState, action ) => {

  // pick up the action type passed by the dispatcher
  if ( action.type === "INC_COUNTER" ) {

    return {
      ...state,
      counter : state.counter + 1,
    }
  }

  if ( action.type === "ADD_COUNTER" ) {

    return {
      ...state,
      counter : state.counter + action.value,
    }
  }

  return state
}

// Store
const store = createStore( rootReducer )
console.log( store.getState() )

// Subscription
// the store triggers all subscriptions when ever the state changes ( thus updated )
// subscription passes the state as props to the components
// with subscriptions we don't need to use 'getState' but will inform when there is a change
store.subscribe( () => {
  console.log( "[Subscription]", store.getState() )
} )

// Dispatching Action
// actions dispatched from the components
// 'type' will define which type of action was dispatch and what we should do in the reducer
// 'type' key is internal and need to be passed, along with type more parameters can be passed ( or one other
// object containing all the parameters )
store.dispatch( { type : "INC_COUNTER" } )
store.dispatch( { type : "ADD_COUNTER", value : 10 } )
console.log( store.getState() )