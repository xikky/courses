import React from 'react';
import {Link} from 'gatsby';

import {StyledImg, WbnSlide} from "./styles/HeroSliderStyles";

const Slide = ( { slide, active } ) => (
  <WbnSlide className={active ? 'active' : ''}>
    <StyledImg fluid={slide.localFile.childImageSharp.fluid}/>
    <div className="wbn-overlay-text">
      <h1 className="wbn-header">
        {slide.title}
      </h1>
      <p className="wbn-text">
        {slide.caption}
      </p>
      <Link to={slide.localFile.relativePath}>
        <button type="button">Button Text</button>
      </Link>
    </div>
  </WbnSlide>
)

export default Slide