import React from "react"
import ReactDOM from "react-dom"
// ##1
// import { createStore } from "redux"
import { createStore, combineReducers } from "redux"
//npm install react-redux
import { Provider } from "react-redux"

import "./index.css"
import App from "./App"
import registerServiceWorker from "./registerServiceWorker"
// ##1
// import reducer from "./store/reducer"
import counterReducer from "./store/reducers/counter"
import resultReducer from "./store/reducers/result"

// ##1
// const store = createStore( reducer )

const rootReducer = combineReducers( {
                                       ctr : counterReducer,
                                       res : resultReducer,
                                     } )

const store = createStore( rootReducer )

//store prop is a special property - expected by the Provider component
ReactDOM.render( <Provider store={store}><App/></Provider>, document.getElementById( "root" ) )
registerServiceWorker()
