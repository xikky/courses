// ##1
// import React from 'react'
//##9
//import React, {Component} from "react"
//use PureComponent when you are using shouldComponentUpdate and checking for ALL properties if changed
import React, {PureComponent} from "react"
import Person from "./Person/Person"

// ##1
// const persons = ( props ) => props.persons.map( ( person, index ) => {
//
//   return <Person
//     click={() => props.clicked( index )}
//     name={person.name}
//     age={person.age}
//     //react needs a unique identifier in the 'key' prop to identify the element. Normally DB id is used
//     //this helps to updating the DOM in a more efficient way
//     key={person.id}
//     changed={( event ) => props.changed( event, person.id )}/>
// } )
//
// export default persons

//##9
//class Persons extends Component {
class Persons extends PureComponent {

    constructor(props) {
        super(props)
        console.log("[Persons.js] inside constructor", props)
    }

    static getDerivedStateFromProps(props, state) {
        console.log('[Persons.js] getDerivedStateFromProps');
        return state;
    }

    componentWillMount() {
        console.log("[Persons.js] inside componenetWillMount")
    }

    componentDidMount() {
        console.log("[Persons.js] inside componenetDidMount")
    }

    //legacy hook
    componentWillReceiveProps(nextProps) {
        console.log("[UPDATE Persons.js] inside componentWillReceiveProps", nextProps)
    }

    //##9
    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log("[UPDATE Persons.js] inside shouldComponentUpdate", nextProps, nextState, this.props.persons)
    //     return nextProps.persons !== this.props.persons ||
    //         nextProps.changed !== this.props.changed ||
    //         nextProps.clicked !== this.props.clicked
    // }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('[Persons.js] getSnapshotBeforeUpdate');
    }

    //legacy
    componentWillUpdate(nextProps, nextState) {
        console.log("[UPDATE Persons.js] inside componentWillUpdate", nextProps, nextState)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("[UPDATE Persons.js] inside componentDidUpdate")
    }

    render() {
        console.log("[Persons.js] inside render")

        return this.props.persons.map((person, index) => {
            return <Person
                click={() => this.props.clicked(index)}
                name={person.name}
                age={person.age}
                key={person.id}
                changed={(event) => this.props.changed(event, person.id)}
                // ##14
                // isAuth={this.props.isAuthenticated}
            />
        });
    }

    componentWillUnmount() {
        console.log('[Persons.js] componentWillUnmount');
    }
}

export default Persons