import React from "react"
import SEO from "../components/seo"

import Layout from '../components/Layout'
import HeroSlider from '../components/index/HeroSlider'
import CTAImages from '../components/index/CTAImages'
import LatestTrend from '../components/index/LatestTrend'
import Citation from '../components/index/Citation'
import About from '../components/index/About'

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[ 'tango', 'brand', 'alliance' ]}/>
    <HeroSlider/>
    <CTAImages/>
    <LatestTrend/>
    <Citation/>
    <About/>
  </Layout>
)

export default IndexPage