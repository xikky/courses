import React from 'react'
//##1
// import Radium from 'radium'
import './Person.css'

//This is a function-based component
//In App we find the class based component example
//function based cannot use state (Stateless)
const person = ( props ) => {

  //##1
  // const style = {
  //   '@media (min-width: 500px)' : {
  //     width : '450px'
  //   }
  // }

  //return <p>I'm a person and I am {Math.floor( Math.random() * 30 )} years old!</p>
  return (
    /*##1*/
    /*<div className="Person" style={style}>*/
    <div className="Person">
      <p onClick={props.click}>I'm {props.name} and I am {props.age} years old!</p>
      <p>{props.children}</p>
      <input type="text" onChange={props.changed} value={props.name}/>
    </div>
  )
}

//##1
// export default Radium( person )
export default person