import React from 'react';
import {navigate} from 'gatsby';

import Header from '../components/Header';

//function with curly brackets needs a return. function with parentheses is the actual return.
const Test = () => (
  <div>
    <Header title="Header of Test Page"/>
    <h1>This is a test page!</h1>
    <button onClick={() => navigate( "/" )}>Navigate to Home</button>
  </div>
)

export default Test;