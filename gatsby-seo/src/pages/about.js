import React from "react"

import PrimaryLayout from "../layouts/PrimaryLayout"

export default () => (
  <PrimaryLayout>
    <h1>About Us</h1>
    <p>
      Meatball ball tip short loin meatloaf boudin spare ribs burgdoggen ham brisket filet mignon tenderloin alcatra. Short loin hamburger drumstick jerky shankle sausage. Boudin meatball pastrami, drumstick kielbasa shankle flank. Turducken fatback swine ground round corned beef. Brisket capicola kielbasa bacon shankle tri-tip spare ribs. Pig tenderloin brisket venison.

      Doner pig shankle, turkey kevin t-bone tri-tip shoulder drumstick strip steak bacon jowl sausage. Boudin fatback jerky pork meatball salami drumstick kevin, t-bone tongue picanha. Turkey turducken prosciutto, sausage alcatra ground round brisket fatback pastrami pork buffalo swine andouille shank kevin. Pork chop biltong drumstick strip steak meatloaf alcatra landjaeger capicola prosciutto. Leberkas cow tongue, pork t-bone boudin shoulder. Ham cupim filet mignon frankfurter shankle rump chicken beef picanha leberkas strip steak pork burgdoggen. Picanha tenderloin tri-tip, cupim pig rump doner turkey short ribs pork flank.
    </p>
  </PrimaryLayout>
)
