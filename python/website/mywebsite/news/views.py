from django.shortcuts import render, get_object_or_404, redirect
from main.models import Main
from .models import News
from django.core.files.storage import FileSystemStorage
import datetime
from subcat.models import SubCat

# Create your views here.

# def news_details(request, pk):


def news_details(request, word):

    site = Main.objects.get(pk=2)
    # news = News.objects.filter(pk=pk)
    news = News.objects.filter(name=word)

    return render(request, 'front/news_details.html', {'news': news, 'site': site})


def news_list(request):

    news = News.objects.all()

    return render(request, 'back/news_list.html', {'news': news})


def news_add(request):

    cat = SubCat.objects.all()

    if request.method == 'POST':

        newstitle = request.POST.get('newstitle')
        newstxtshort = request.POST.get('newstxtshort')
        newstxt = request.POST.get('newstxt')
        newscatid = request.POST.get('newscat')

        if newstitle == "" or newstxtshort == "" or newstxt == "" or newscatid == "":
            error = "All Fields Required!"
            return render(request, 'back/error.html', {'error': error})

        try:

            myfile = request.FILES['myfile']

            # same as request.FILES['myfile']
            if str(request.FILES.get('myfile').content_type).startswith("image"):

                if myfile.size < 5000000:
                    fs = FileSystemStorage()
                    filename = fs.save(myfile.name, myfile)
                    url = fs.url(filename)

                    date = datetime.datetime.now()

                    newscat = SubCat.objects.get(pk=newscatid).name

                    b = News(name=newstitle, short_txt=newstxtshort, body_txt=newstxt,
                             date=date.strftime('%d/%m/%Y'), picname=filename, picurl=url, writer="-", cat=newscat, catid=newscatid)
                    b.save()
                    return redirect('news_list')
                else:
                    error = "Your Image is bigger than 5MB"
                    return render(request, 'back/error.html', {'error': error})

            else:
                error = "Your File is not supported"
                return render(request, 'back/error.html', {'error': error})
        except:
            error = "Please input your image"
            return render(request, 'back/error.html', {'error': error})

    return render(request, 'back/news_add.html', {'cat': cat})


def news_delete(request, pk):

    try:

        # b = News.objects.get(pk=pk)
        # with filter, it searches for the pk, and if it doesn't find, it just returns an empty Dic so no error is created if a non
        # existing pk is manually inserted in the address bar
        b = News.objects.filter(pk=pk)

        fs = FileSystemStorage()
        fs.delete(b[0].picname)

        b.delete()
    except:

        error = "something Wrong"
        return render(request, 'back/error.html', {'error': error})

    return redirect('news_list')
