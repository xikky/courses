import React, { PureComponent } from "react"
//##7
// import React, {Component} from 'react';
// importing css files
// import './App.css';
// importing css files scoped to this module only
import classes from "./App.module.css"
// ##4
// import Radium, {StyleRoot} from 'radium';

//The below code is removed since these components are moved into component - Persons
// import Person from '../components/Persons/Person/Person'
// import ErrorBoundary from '../ErrorBoundary/ErrorBoundary'

import Persons from "../components/Persons/Persons"
import Cockpit from "../components/Cockpit/Cockpit"
//##11
//##12
// import WithClass from '../hoc/WithClass';
// import using lowerCamelCase instead of UpperCamelCase since it's a normal JS function now and not a component
import withClass from "../hoc/withClass"
import Aux from "../hoc/Auxiliary"

import AuthContext from "../context/auth-context"

//This is a class based component. Any properties can be grabbed by using `this.props`
//In person we find the function based component example
class App extends PureComponent {
//##7
// class App extends Component {

  constructor ( props ) {
    //execute the constructor of the component extended
    super( props )
    console.log( "[App.js] inside constructor", props )

    //can access state here (and initialize) using this.state.
  }

  componentWillMount () {
    console.log( "[App.js] inside componenetWillMount" )
  }

  componentDidMount () {
    console.log( "[App.js] inside componenetDidMount" )
  }

  //##7
  // shouldComponentUpdate ( nextProps, nextState ) {
  //   console.log( '[UPDATE App.js] inside shouldComponentUpdate', nextProps, nextState )
  //   return nextState.persons !== this.state.persons ||
  //     nextState.showPersons !== this.state.showPersons
  // }

  componentWillUpdate ( nextProps, nextState ) {
    console.log( "[UPDATE App.js] inside componentWillUpdate", nextProps, nextState )
  }

  componentDidUpdate () {
    console.log( "[UPDATE App.js] inside componentDidUpdate" )
  }

  //state can only be used in class based components
  state = {
    persons       : [
      { id : 1, name : "Max", age : 28 },
      { id : 2, name : "Manu", age : 29 },
      { id : 3, name : "Daniel", age : 24 },
    ],
    showPersons   : false,
    changeCounter : 0,
    authenticated : false,
  }

  static getDerivedStateFromProps ( props, state ) {

    console.log( "[App.js] getDerivedStateFromProps", props )
    return state
  }

  //##3
  // switchNameHandler = ( newName ) => {
  //
  //   //DON'T: this.state.persons[ 0 ].name = 'Maximus' use setState
  //   this.setState( {
  //                    persons : [
  //                      { name : newName, age : 38 },
  //                      { name : 'Manu', age : 39 },
  //                      { name : 'Daniel', age : 34 }
  //                    ]
  //                  } )
  // }

  //##3
  // nameChangedHandler = ( event ) => {
  //
  //   this.setState( {
  //                    persons : [
  //                      { name : 'Max', age : 38 },
  //                      { name : event.target.value, age : 39 },
  //                      { name : 'Daniel', age : 34 }
  //                    ]
  //                  } )
  // }

  nameChangedHandler = ( event, id ) => {

    //findIndex returns the index of the element. find returns the element
    const personIndex = this.state.persons.findIndex( person => {

      //if true return it into const personIndex
      return person.id === id
    } )

    //this will only copy the pointer since objects in JS are referenced
    //const person = this.state.person[personIndex]
    //using spread (...) we extract the elements from the persons object selected by the index
    const person = {
      ...this.state.persons[ personIndex ],
    }

    //event.target picks the elment where the event is coming from
    person.name = event.target.value

    const persons = [ ...this.state.persons ]
    persons[ personIndex ] = person

    //When using setState to change values depending on the state itself, pass the prevState and props parameters
    this.setState( ( prevState, props ) => {
      return {
        persons       : persons,
        changeCounter : prevState.changeCounter + 1,
      }
    } )
  }

  togglePersonsHandler = () => {

    const doesShow = this.state.showPersons
    this.setState( { showPersons : !doesShow } )
  }

  deletePersonHandler = ( index ) => {

    //Below saves just a pointer of persons, so the actual object will be manipulated
    //const persons = this.state.persons
    //For better practice copy the whole object instead of just the pointer as the examples below
    //const persons = this.state.persons.slice()
    const persons = [ ...this.state.persons ] //This spreads the contents of the objects and stores them in the array []
    persons.splice( index, 1 )
    this.setState( { persons : persons } )
  }

  loginHandler = () => {
    this.setState( { authenticated : true } )
  }

  render () {
    console.log( "[App.js] inside render" )
    // ##5
    // const style = {
    //   backgroundColor : 'green',
    //   color           : 'white',
    //   font            : 'inherit',
    //   border          : '1px solid blue',
    //   padding         : '8px',
    //   cursor          : 'pointer',
    //css pseudo names work with Radium package
    //##4
    // ':hover'        : {
    //   backgroundColor : 'lightgreen',
    //   color           : 'black'
    // }
    //##5
    // }

    let persons = null
    // The below code is moved to another component - Cockpit
    // let btnClass = ''

    if ( this.state.showPersons ) {
      persons = (<Persons
          persons={this.state.persons}
          clicked={this.deletePersonHandler}
          changed={this.nameChangedHandler}
          //##14
          // isAuthenticated={this.state.authenticated}
        />

        //The below code is moved to another component - Persofns
        //<div>
        //  {this.state.persons.map( ( person, index ) => {
        //
        //  return <ErrorBoundary key={person.id}>/!*##6*!/
        //  <Person
        //  click={this.deletePersonHandler.bind( this, index )}
        //  name={person.name}
        //  age={person.age}
        //  //react needs a unique identifier in the 'key' prop to identify the element. Normally DB id is used
        //  //this helps to updating the DOM in a more efficient way
        //  //##6
        //  // key={person.id}
        //  changed={( event ) => this.nameChangedHandler( event, person.id )}/>
        //  </ErrorBoundary>
        //  } )}
        //</div>
      )

      //##5
      // style.backgroundColor = 'red';
      //css pseudo names work with Radium package
      //##4
      // style[ ':hover' ] = {
      //   backgroundColor : 'salmon',
      //   color           : 'black'
      // }

      // The below code is moved to another component - Cockpit
      // btnClass = classes.Red
    }

    // The below code is moved to another component - Cockpit
    // //let classes = [ 'red', 'bold' ].join( ' ' )
    // const assignedClasses = []
    //
    // if ( this.state.persons.length <= 2 ) {
    //   // insert classes from css file ( before using css module scoping )
    //   // classes.push( 'red' )
    //   assignedClasses.push( classes.red )
    // }
    // if ( this.state.persons.length <= 1 ) {
    //   // insert classes from css file ( before using css module scoping )
    //   // classes.push( 'bold' )
    //   assignedClasses.push( classes.bold )
    // }

    return (
      <Aux>
        {/*##4*/}
        {/*<StyleRoot>*/}
        {/*div with class="App" from imported css file ( before using css module scoping )*/}
        {/*<div className="App">*/}
        {/*//##11*/}
        {/*<div className={classes.App}>*/}
        {/*//##12*/}
        {/*<WithClass classes={classes.App}>*/}
        <button onClick={() => {
          this.setState( { showPersons : true } )
        }}>Show Persons
        </button>
        <AuthContext.Provider
          value={{
            authenticated : this.state.authenticated,
            login         : this.loginHandler,
          }}
        >
          <Cockpit
            //this.props is provided by React when a property is passed to a component ( App in this case inside index.js ) which is a class ( stateful component )
            appTitle={this.props.title}
            showPersons={this.state.showPersons}
            //##8
            //persons={this.state.persons}
            personsLength={this.state.persons.length}
            clicked={this.togglePersonsHandler}
            //##14
            // login={this.loginHandler}
          />
        </AuthContext.Provider>
        {/*The below code is moved to another component - Cockpit*/}
        {/*<h1>Hi, I am Daniel</h1>*/}
        {/*<p className={assignedClasses.join( ' ' )}>I am a developer!</p>*/}
        {/*<button*/}
        {/*className={btnClass}*/}
        {/*##5*/}
        {/*/}/ style={style}*/}
        {/*ideally use bind function as example below*/}
        {/*() => return this.f( 'Reuben' )*/}
        {/*/}/ onClick={() => this.switchNameHandler( 'Reuben' )}>*/}
        {/*onClick={this.togglePersonsHandler}>*/}
        {/*Switch Name*/}
        {/*</button>*/}

        {/*##1*/}
        {/*<Person name="Max" age="28">My Hobbies: Racing</Person>*/}

        {/*##2*/}
        {/*<Person*/}
        {/*name={this.state.persons[ 0 ].name}*/}
        {/*age={this.state.persons[ 0 ].age}*/}
        {/*>My Hobbies: Racing</Person>*/}

        {/*##3*/}
        {/*<Person*/}
        {/*name={this.state.persons[ 1 ].name}*/}
        {/*age={this.state.persons[ 1 ].age}*/}
        {/*click={this.switchNameHandler.bind( this, 'Joseph' )}*/}
        {/*changed={this.nameChangedHandler}>My Hobbies: Racing</Person>*/}

        {persons}
        {/*##11*/}
        {/*</div>*/}
        {/*##12*/}
        {/*</WithClass>*/}
        {/*##4*/}
        {/*</StyleRoot>*/}
      </Aux>
    )

    //this is why we need to import React, because this is what is happening behind the scenes
    //return React.createElement( 'div', null, React.createElement( 'h1', null, 'My name is Daniel') )
  }
}

//##4
//export default Radium( App );
//##12
// export default App;
export default withClass( App, classes.App )