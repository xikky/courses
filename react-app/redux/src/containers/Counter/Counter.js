import React, { Component } from "react"
import { connect } from "react-redux"
import * as actionTypes from "../../store/actions"

import CounterControl from "../../components/CounterControl/CounterControl"
import CounterOutput from "../../components/CounterOutput/CounterOutput"

class Counter extends Component {
  state = {
    counter : 0,
  }

  counterChangedHandler = ( action, value ) => {
    switch ( action ) {
      case "inc":
        this.setState( ( prevState ) => {
          return { counter : prevState.counter + 1 }
        } )
        break
      case "dec":
        this.setState( ( prevState ) => {
          return { counter : prevState.counter - 1 }
        } )
        break
      case "add":
        this.setState( ( prevState ) => {
          return { counter : prevState.counter + value }
        } )
        break
      case "sub":
        this.setState( ( prevState ) => {
          return { counter : prevState.counter - value }
        } )
        break
      default:
        break
    }
  }

  render () {
    return (
      <div>
        <CounterOutput value={this.props.ctr}/>
        {/*<CounterOutput value={this.state.counter}/>*/}
        <CounterControl label="Increment" clicked={this.props.onIncrementCounter}/>
        {/*<CounterControl label="Increment" clicked={() => this.counterChangedHandler("inc")}/>*/}
        <CounterControl label="Decrement" clicked={this.props.onDecrementCounter}/>
        {/*<CounterControl label="Decrement" clicked={() => this.counterChangedHandler("dec")}/>*/}
        <CounterControl label="Add 10" clicked={this.props.onAddCounter}/>
        {/*<CounterControl label="Add 5" clicked={() => this.counterChangedHandler("add", 5)}/>*/}
        <CounterControl label="Subtract 10" clicked={this.props.onSubtractCounter}/>
        {/*<CounterControl label="Subtract 5" clicked={() => this.counterChangedHandler("sub", 5)}/>*/}
        <hr/>
        {/*##1*/}
        {/*<button onClick={this.props.onStoreResult}>Store Result</button>*/}
        <button onClick={() => this.props.onStoreResult( this.props.ctr )}>Store Result</button>
        <ul>
          {this.props.storedResults.map( strResult => (
            //onDeleteResult can have parenthesis since it won't be executed at the point of time the component renders
            //because it is wrapped in a function
            <li key={strResult.id} onClick={() => this.props.onDeleteResult( strResult.id )}>{strResult.value}</li>
          ) )}
        </ul>
      </div>
    )
  }
}

// the state here is provided by redux, coming from the reducer
const mapStateToProps = state => {

  // ##1
  // return {
  //     ctr: state.counter,
  //     storedResults: state.results
  // }

  //after using combinedReducers state variables need to change to access the sub states declared.
  return {
    ctr           : state.ctr.counter,
    storedResults : state.res.results,
  }
}

const mapDispatchToProps = dispatch => {

  return {
    onIncrementCounter : () => dispatch( { type : actionTypes.INCREMENT } ),
    onDecrementCounter : () => dispatch( { type : actionTypes.DECREMENT } ),
    onAddCounter       : () => dispatch( { type : actionTypes.ADD, value : 10 } ),
    onSubtractCounter  : () => dispatch( { type : actionTypes.SUBTRACT, value : 10 } ),
    // ##1
    // onStoreResult      : () => dispatch( { type : actionTypes.STORE_RESULT } ),
    // the counter needs now to be passed as a parameter since we don't have access to sub states from other reducers
    onStoreResult      : ( result ) => dispatch( { type : actionTypes.STORE_RESULT, result : result } ),
    onDeleteResult     : ( id ) => dispatch( { type : actionTypes.DELETE_RESULT, resultElID : id } ),
  }
}

// connect is a hoc, but is used as a function because it returns a function
export default connect( mapStateToProps, mapDispatchToProps )( Counter )