import React, {useEffect, useRef, useContext} from 'react'
import classes from './Cockpit.module.css'

import AuthContext from '../../context/auth-context'

const cockpit = (props) => {

    const toggleBtnRef = useRef(null);

    //less verbose function type use of context ( ##15 )
    const authContext = useContext(AuthContext);

    console.log(authContext.authenticated);

    //same as Lifecycle Hook componentsDidUpdate - but this is a new React Hook ( useEffect can be used multiple times )
    //second parameter ( after callback ) is used to check if something changed. If it is, execute callback.
    //the second parameter can be an empty array '[]' to run useEffect only once. This tells react this effect has no dependencies
    //and it should rerun when one of these dependencies ( like props.persons ) changes. If there is no dependencies hence
    // it can never rerun.
    useEffect(() => {
        console.log('[Cockpit.js] useEffect');
        // HTTP request..

        //using setTimeout to fake an http request
        setTimeout(() => {
            alert('Saved data to cloud');
        }, 1000)

        //this is executed before any other logic in useEffect ( when useEffect is triggered )
        //useful to check if you might need to cancel the useEffect logic
        return () => {
            console.log('[Cockpit.js] clean up in useEffect');
        }
    }, [props.persons]);

    useEffect(() => {
        toggleBtnRef.current.click();
    }, []);

    const assignedClasses = []
    let btnClass = ''

    if (props.showPersons) {

        btnClass = classes.Red
    }

    //##8
    //if (props.persons.length <= 2) {
    if (props.personsLength <= 2) {

        assignedClasses.push(classes.red)
    }

    //##8
    //if (props.persons.length <= 1) {
    if (props.personsLength <= 1) {

        assignedClasses.push(classes.bold)
    }

    return (
        <div className={classes.Cockpit}>
            <h1>{props.appTitle}</h1>
            <p className={assignedClasses.join(' ')}>I am a developer!</p>
            <button
                ref={toggleBtnRef}
                className={btnClass}
                onClick={props.clicked}>
                Switch Name
            </button>
            <button onClick={authContext.login}>Log in</button>#
            {/*##15*/}
            {/*<AuthContext.Consumer>*/}
            {/*    {(context) => {*/}
            {/*        <button onClick={context.login}>Log in</button>*/}
            {/*    }}*/}
            {/*</AuthContext.Consumer>*/}
            {/*##14*/}
            {/*<button onClick={props.login}>Log in</button>*/}
        </div>
    )
}

//##8
//export default cockpit
//.memo is the PureComponent replacement for functional components. Used to update the component by checking all
// props and only if something changed, the component will update.
export default React.memo(cockpit);