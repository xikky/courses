import React from "react"
import ReactDOM from "react-dom"
import "./index.css"
import App from "./App"
import registerServiceWorker from "./registerServiceWorker"
import axios from "axios"

//axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';
axios.defaults.headers.common['Authorization'] = 'Auth Token';
axios.defaults.headers.post['Content-Type'] = 'application/json'; //not needed, by default is json

axios.interceptors.request.use( request => {
  console.log( request )
  //edit request config
  //good to set auth headers
  return request
}, error => {
  console.log( error )
  //the error here is related to setting the request, like no internet connectivity
  return Promise.reject( error )
} )

axios.interceptors.response.use( response => {
  console.log( response )
  //edit response config
  return response
}, error => {
  console.log( error )
  //the error here is on response, like 404 URL
  return Promise.reject( error )
} )

ReactDOM.render( <App/>, document.getElementById( "root" ) )
registerServiceWorker()
