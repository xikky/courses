import React from 'react'

import tangoFB from '../images/tango-facebook-icon.svg';
import tangoIG from '../images/tango-instagram-icon.svg';
import tangoLI from '../images/tango-linkedin-icon.svg';

import {FooterWrapper} from "./styles/FooterStyles";

const Footer = () => (
  <FooterWrapper>
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1 className="whiteText">
            CONTACT
          </h1>
        </div>
      </div>
      <div className="row">
        <div className="col-md-4">
          <h3>Daniel Scicluna</h3>
          <p className="tango-contact">
            <a href="mailto:dannyscicluna@gmail.com">
              dannyscicluna@gmail.com
            </a>
          </p>
          <p className="tango-contact">
            +46 (0) 706 531 992
          </p>
          <span className="social">
            <a
              target="_blank"
              rel="noopener norefferer"
              href="http://www.google.com"
            >
              <img src={tangoLI} alt="tango-li"/>
            </a>
          </span>
        </div>
        <div className="col-md-4">
          <h3>Daniel Scicluna</h3>
          <p className="tango-contact">
            <a href="mailto:dannyscicluna@gmail.com">
              dannyscicluna@gmail.com
            </a>
          </p>
          <p className="tango-contact">
            +46 (0) 706 531 992
          </p>
          <span className="social">
            <a
              target="_blank"
              rel="noopener norefferer"
              href="http://www.google.com"
            >
              <img src={tangoLI} alt="tango-li"/>
            </a>
          </span>
        </div>
        <div className="col-md-4">
          <h3>Daniel Scicluna</h3>
          <p className="tango-contact">
            <a href="mailto:dannyscicluna@gmail.com">
              dannyscicluna@gmail.com
            </a>
          </p>
          <p className="tango-contact">
            +46 (0) 706 531 992
          </p>
          <span className="social">
            <a
              target="_blank"
              rel="noopener norefferer"
              href="http://www.google.com"
            >
              <img src={tangoLI} alt="tango-li"/>
            </a>
          </span>
        </div>
      </div>
    </div>
  </FooterWrapper>
)

export default Footer;