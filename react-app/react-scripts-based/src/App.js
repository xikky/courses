import React, {Component} from 'react';
import './App.css';
// ##4
// import Radium, {StyleRoot} from 'radium';
import Person from './Person/Person';

//This is a class based component. Any properties can be grabbed by using `this.props`
//In person we find the function based component example
class App extends Component {

  //state can only be used in class based components
  state = {
    persons     : [
      { id : 1, name : 'Max', age : 28 },
      { id : 2, name : 'Manu', age : 29 },
      { id : 3, name : 'Daniel', age : 24 }
    ],
    showPersons : false
  }

  //##3
  // switchNameHandler = ( newName ) => {
  //
  //   //DON'T: this.state.persons[ 0 ].name = 'Maximus' use setState
  //   this.setState( {
  //                    persons : [
  //                      { name : newName, age : 38 },
  //                      { name : 'Manu', age : 39 },
  //                      { name : 'Daniel', age : 34 }
  //                    ]
  //                  } )
  // }

  //##3
  // nameChangedHandler = ( event ) => {
  //
  //   this.setState( {
  //                    persons : [
  //                      { name : 'Max', age : 38 },
  //                      { name : event.target.value, age : 39 },
  //                      { name : 'Daniel', age : 34 }
  //                    ]
  //                  } )
  // }

  nameChangedHandler = ( event, id ) => {

    //findIndex returns the index of the element. find returns the element
    const personIndex = this.state.persons.findIndex( person => {

      //if true return it into const personIndex
      return person.id === id
    } )

    //this will only copy the pointer since objects in JS are referenced
    //const person = this.state.person[personIndex]
    //using spread (...) we extract the elements from the persons object selected by the index
    const person = {
      ...this.state.persons[ personIndex ]
    }

    //event.target picks the elment where the event is coming from
    person.name = event.target.value

    const persons = [ ...this.state.persons ]
    persons[ personIndex ] = person

    this.setState( {
                     persons : persons
                   } )
  }

  togglePersonsHandler = () => {

    const doesShow = this.state.showPersons
    this.setState( { showPersons : !doesShow } )
  }

  deletePersonHandler = ( index ) => {

    //Below saves just a pointer of persons, so the actual object will be manipulated
    //const persons = this.state.persons
    //For better practice copy the whole object instead of just the pointer as the examples below
    //const persons = this.state.persons.slice()
    const persons = [ ...this.state.persons ] //This spreads the contents of the objects and stores them in the array []
    persons.splice( index, 1 )
    this.setState( { persons : persons } )
  }

  render () {

    const style = {
      backgroundColor : 'green',
      color           : 'white',
      font            : 'inherit',
      border          : '1px solid blue',
      padding         : '8px',
      cursor          : 'pointer',
      //css pseudo names work with Radium package
      //##4
      // ':hover'        : {
      //   backgroundColor : 'lightgreen',
      //   color           : 'black'
      // }
    }

    let persons = null

    if ( this.state.showPersons ) {
      persons = (
        <div>
          {this.state.persons.map( ( person, index ) => {

            return <Person
              click={this.deletePersonHandler.bind( this, index )}
              name={person.name}
              age={person.age}
              //react needs a unique identifier in the 'key' prop to identify the element. Normally DB id is used
              //this helps to updating the DOM in a more efficient way
              key={person.id}
              changed={( event ) => this.nameChangedHandler( event, person.id )}/>
          } )}
        </div>
      )

      style.backgroundColor = 'red';
      //css pseudo names work with Radium package
      //##4
      // style[ ':hover' ] = {
      //   backgroundColor : 'salmon',
      //   color           : 'black'
      // }
    }

    //let classes = [ 'red', 'bold' ].join( ' ' )
    const classes = []

    if ( this.state.persons.length <= 2 ) {
      classes.push( 'red' )
    }
    if ( this.state.persons.length <= 1 ) {
      classes.push( 'bold' )
    }

    return (
      /*##4*/
      /*<StyleRoot>*/
        <div className="App">
          <h1>Hi, I am Daniel</h1>
          <p className={classes.join( ' ' )}>I am a developer!</p>
          <button
            style={style}
            //() => return this.switchNameHandler( 'Reuben' )} //ideally use bind function as example below
            // onClick={() => this.switchNameHandler( 'Reuben' )}>
            onClick={this.togglePersonsHandler}>
            Switch Name
          </button>

          {/*##1*/}
          {/*<Person name="Max" age="28">My Hobbies: Racing</Person>*/}

          {/*##2*/}
          {/*<Person*/}
          {/*name={this.state.persons[ 0 ].name}*/}
          {/*age={this.state.persons[ 0 ].age}*/}
          {/*>My Hobbies: Racing</Person>*/}

          {/*##3*/}
          {/*<Person*/}
          {/*name={this.state.persons[ 1 ].name}*/}
          {/*age={this.state.persons[ 1 ].age}*/}
          {/*click={this.switchNameHandler.bind( this, 'Joseph' )}*/}
          {/*changed={this.nameChangedHandler}>My Hobbies: Racing</Person>*/}

          {persons}
        </div>
        /*##4*/
      /*</StyleRoot>*/
    );
    //this is why we need to import React, because this is what is happening behind the scenes
    //return React.createElement( 'div', null, React.createElement( 'h1', null, 'My name is Daniel') )
  }
}

//##4
//export default Radium( App );
export default App;

