import React from 'react';
import {useStaticQuery, graphql} from 'gatsby';

import CTA from './CTA';

const CTAImages = () => {
  const {
          cta : { acf : cta },
        } = useStaticQuery( graphql`
      fragment ctaImage on wordpress__wp_media {
          localFile{
              childImageSharp {
                  fluid(quality: 100, maxWidth: 500){
                      ...GatsbyImageSharpFluid_withWebp
                  }
              }
          }
      }
      query {
          cta: wordpressPage(wordpress_id: { eq: 5}){
              acf {
                  text_1
                  link_1{
                      url
                  }
                  image_1{
                      ...ctaImage
                  }
                  text_2
                  link_2{
                      url
                  }
                  image_2{
                      ...ctaImage
                  }
              }
          }
      }
  ` )
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <CTA
              image={cta.image_1.localFile.childImageSharp.fluid}
              link={cta.link_1.url}
              text={cta.text_1}
            />
          </div>
          <div className="col-md-4">
            <CTA
              image={cta.image_2.localFile.childImageSharp.fluid}
              link={cta.link_2.url}
              text={cta.text_2}
            />
          </div>
        </div>
      </div>
    </>
  )
}

export default CTAImages