from __future__ import unicode_literals
from django.db import models

# Create your models here.


class News(models.Model):

    # TextField - textarea
    # CharField - text field

    name = models.CharField(max_length=50)
    short_txt = models.TextField()
    body_txt = models.TextField()
    date = models.CharField(max_length=12)
    picname = models.TextField(default='-')
    picurl = models.TextField(default='-')
    writer = models.CharField(max_length=50)
    cat = models.CharField(max_length=50, default='-')
    catid = models.IntegerField(default=0)
    show = models.IntegerField(default=0)

    def __str__(self):
        return self.name
