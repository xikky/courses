// ##2
//import React from 'react'
import React, { Component } from "react"
//##1
// import Radium from 'radium'
// importing css files
// import './Person.css'
// importing css files scoped to this module only

import PropTypes from "prop-types"

//##10
//import Aux from '../../../hoc/Auxiliary';
import withClass from "../../../hoc/withClass"
import classes from "./Person.module.css"

import AuthContext from "../../../context/auth-context"

// ##2
// //This is a function-based component
// //In App we find the class based component example
// //function based cannot use state (Stateless)
// const person = ( props ) => {
//
//   //Below 5 lines of code are used to test ErrorBoundary
//   //This feature only displays in production mode
//   //Error Boundaries should only be used when the developer knows some code might fail, so don't wrap everything
//   //within error boundaries
//   // const rnd = Math.random()
//
//   //ErrorBoundary stopped from example
//   // if ( rnd > 0.7 ) {
//   //   throw new Error( 'Something went wrong' )
//   // }
//
//   //##1
//   // const style = {
//   //   '@media (min-width: 500px)' : {
//   //     width : '450px'
//   //   }
//   // }
//
//   //return <p>I'm a person and I am {Math.floor( Math.random() * 30 )} years old!</p>
//   return (
//     /*##1*/
//     /*<div className="Person" style={style}>*/
//     /*div with class="Person" from imported css file ( before using css module scoping )*/
//     /*<div className="Person">*/
//     <div className={classes.Person}>
//       <p onClick={props.click}>I'm {props.name} and I am {props.age} years old!</p>
//       <p>{props.children}</p>
//       <input type="text" onChange={props.changed} value={props.name}/>
//     </div>
//   )
// }
//
// //##1
// // export default Radium( person )
// export default person

class Person extends Component {

  constructor ( props ) {
    super( props )
    console.log( "[Person.js] inside constructor", props )
    this.inputElementRef = React.createRef()
  }

  //less verbose class type use of context ( ##15 )
  static contextType = AuthContext

  componentWillMount () {
    console.log( "[Person.js] inside componenetWillMount" )
  }

  componentDidMount () {
    console.log( "[Person.js] inside componenetDidMount" )
    //inputElement is declared since componentDidMount runs after render()
    // ##13
    // this.inputElement.focus();
    //reference can also be created from constructor
    this.inputElementRef.current.focus()
    console.log( this.context.authenticated )
  }

  //##10
  render () {
    console.log( "[Person.js] inside render" )
    return (
      //Using Aux will mitigate the need to have a root component in our HTML. This works
      //because React needs to always have one expression returned rather then multiple elements,
      //hence the solution with the Aux component.
      /*<Aux>*/
      //Since React 16.2 the Aux is built and can be used like so. To not use the dot notation, import
      //Fragment just like Component
      <React.Fragment>
        {this.context.authenticated ? <p>Authenticated!</p> : <p>Please log in.</p>}
        {/*##15*/}
        {/*<AuthContext.Consumer>*/}
        {/*{(context) => context.authenticated ? <p>Authenticated!</p> : <p>Please log in.</p>}*/}
        {/*</AuthContext.Consumer>*/}
        {/*##14*/}
        {/*{this.props.isAuth ? <p>Authenticated!</p> : <p>Please log in.</p>}*/}
        <p onClick={this.props.click}>I'm {this.props.name} and I am {this.props.age} years old!</p>
        <p>{this.props.children}</p>
        <input type="text"
          // ##13
          // ref={(inputEl) => {
          //     this.inputElement = inputEl
          // }}
               ref={this.inputElementRef}
               onChange={this.props.changed} value={this.props.name}/>
        {/*</Aux>*/}
      </React.Fragment>
    )
  }

  //##10
  // render() {
  //     console.log('[Person.js] inside render');
  //     return (
  //         <div className={classes.Person}>
  //             <p onClick={this.props.click}>I'm {this.props.name} and I am {this.props.age} years old!</p>
  //             <p>{this.props.children}</p>
  //             <input type="text" onChange={this.props.changed} value={this.props.name}/>
  //         </div>
  //     )
  // }

  //React does not allow us to send adjacent elements, meaning only one root element inside the render function.
  //Under the hood, this elements are given a key so React can update them efficiently. Hence if instead an array of
  //elements is returned in the render function with a manually inserted unique key attribute, it will update the DOM
  //as normal without any warnings
  // render() {
  //     return [
  //         <p key="item1" onClick={this.props.click}>I'm {this.props.name} and I am {this.props.age} years old!</p>,
  //         <p key="item2">{this.props.children}</p>,
  //         <input key="item3" type="text" onChange={this.props.changed} value={this.props.name}/>
  //     ]
  // }
}

Person.propTypes = {
  click  : PropTypes.func,
  name   : PropTypes.string,
  age    : PropTypes.number,
  change : PropTypes.func,
}

export default withClass( Person, classes.Person )