module.exports = {
  siteMetadata : {
    title       : `The Gatsby Wordpress Course`,
    description : `A code example to learn using gatsby with wordpress`,
    author      : `@gatsbyjs`,
  },
  plugins      : [
    `gatsby-plugin-react-helmet`,
    {
      resolve : `gatsby-source-filesystem`,
      options : {
        name : `images`,
        path : `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve : `gatsby-plugin-manifest`,
      options : {
        name             : `gatsby-starter-default`,
        short_name       : `starter`,
        start_url        : `/`,
        background_color : `#639`,
        theme_color      : `#639`,
        display          : `minimal-ui`,
        icon             : `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve : 'gatsby-source-wordpress',
      options : {
        excludedRoutes              : [
          '/mds/v1/playcasino*',
          '/wp/v2/users/**',
          '/wp/v2/themes*',
          '/wp/v2/settings*'
        ],
        baseUrl                     : 'reactpress.loc',
        protocol                    : 'http',
        hostingWPCOM                : false,
        useACF                      : true,
        searchAndReplaceContentUrls : {
          sourceUrl      : 'http://reactpress.loc',
          replacementUrl : ''
        }
      }
    },
    'gatsby-plugin-styled-components',
    {
      resolve : 'gatsby-plugin-prefetch-google-fonts',
      options : {
        fonts : [
          {
            family   : 'Teko',
            variants : [ '200', '400', '500', '600', '700' ]
          }
        ]
      }
    }
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
