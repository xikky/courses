import * as actionTypes from "../actions"

const initialScale = {
  results: []
}

const reducer = (state = initialScale, action) => {
console.log(action);
  switch (action.type) {
    case actionTypes.STORE_RESULT:
      // ##1
      // state.ctr.counter -> here .ctr would be undefined because inside this reducer function, it
      // has no access to the global state but only to the state passed
      // return {
      //     ...state,
      //     results: state.results.concat({id: new Date(), value: state.counter})
      // }
      return {
        ...state,
        results: state.results.concat({id: new Date(), value: action.result})
      }
    case actionTypes.DELETE_RESULT:

      //how to change an array immutably
      // const id = 2;
      // const newArray = [...state.results];
      // state.results.splice(id, 1);

      //filter puts in the updatedArray all elements that return true
      const updatedArray = state.results.filter(result => result.id !== action.resultElID);

      return {
        ...state,
        results: updatedArray
      }
    default:
      return state
  }
}

export default reducer