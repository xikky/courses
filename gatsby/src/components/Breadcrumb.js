import React from 'react';
import {Link} from 'gatsby';
import {PropTypes} from 'prop-types';

import {BreadCrumbWrapper} from "./styles/BreadCrumbStyles";

const Breadcrumb = ( { parent } ) => (
  <div className="container">
    <div className="row">
      <div className="col-lg-9 offset-lg-3">
        <BreadCrumbWrapper>
          <Link to="/">
            <span>Tango Brand Alliance</span>
          </Link>
          <span className="divider">/</span>
          {parent && parent != 0 ? (
            <>
              <Link to={parent.link} alt={parent.title}>
                <span dangerouslySetInnerHTML={{ __html : parent.title }}></span>
              </Link>
              <span className="divider">/</span>
            </>
          ) : null}
        </BreadCrumbWrapper>
      </div>
    </div>
  </div>
)

Breadcrumb.propTypes = {
  parent : PropTypes.object
}

export default Breadcrumb;