/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  siteMetadata : {
    title       : "Gatsby-Bootstrap",
    description : 'This is the description of our website',
    keywords    : 'gatsby, gastbyjs',
    image       : '/static/gatsby.jpg',
    url         : 'https://www.gatsbyjs.org'
  },
  plugins      : [
    {
      resolve : `gatsby-source-filesystem`,
      options : {
        name : `src`,
        path : `${__dirname}/src/`,
      },
    },
    {
      resolve : `gatsby-transformer-remark`,
      options : {
        // CommonMark mode (default: true)
        commonmark : true,
        // Footnotes mode (default: true)
        footnotes  : true,
        // Pedantic mode (default: true)
        pedantic   : true,
        // GitHub Flavored Markdown mode (default: true)
        gfm        : true,
        // Plugins configs
        plugins    : [],
      },
    },
    {
      resolve : `gatsby-source-wordpress`,
      options : {
        baseUrl      : `reactpress.loc`,
        protocol     : 'http',
        hostingWPCOM : false,
      }
    },
    `gatsby-plugin-react-helmet`
  ],
}
